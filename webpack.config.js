module.exports = {
    entry: {
        index: "./js/entry.js",
        profile: "./js/profile.js",
    },
    output: {
		path: __dirname,
        filename: "js/build/[name].js"
    }
};