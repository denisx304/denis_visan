module.exports = function SwipeableMenu() {
	
	this.container = $(".swipeable-menu-container");
	this.tabs = this.container.children();
	this.menu = $(".swipeable-menu");
	
	this.addDefaultShadow = function() {
		if (this.container.outerWidth() < this.container[0].scrollWidth) {
			this.menu.addClass('shadow-visible-right');
		}
	};
	
	this.getOffset = function(distance, coefficient) {
		return distance * coefficient;
	};	
	
	this.getLeftOffset = function(event, direction) {
		var time = event.swipestop.time - event.swipestart.time;
		var coefficient = 7;
		var left = parseFloat(this.container.css("left"));
		var offset = 0;
		var distance = 0;
		
		var maxOffset = this.container[0].scrollWidth - this.container.outerWidth();
		
		if (direction == "left") {
			distance = event.swipestart.coords[0] - event.swipestop.coords[0];
			offset = Math.max(-maxOffset, (left - this.getOffset(distance, coefficient))) + "px";
		} else if (direction == "right") {
			distance = event.swipestop.coords[0] - event.swipestart.coords[0];
			offset = Math.min((left + this.getOffset(distance, coefficient)), 0) + "px";
		}
		
		return {
			offset: offset,
			duration: time * coefficient / 10
		};
	};
	
	this.container.on("swipe", (event) => {
		var direction = "left";
		
		if (event.swipestart.coords[0] < event.swipestop.coords[0]) {
			direction = "right";
		}
		
		var swipeInfo = this.getLeftOffset(event, direction);
		
		this.container.animate({
			left: swipeInfo.offset
		}, swipeInfo.duration);
	});
	
	this.tabs.click((event) => {
		this.tabs.removeClass("active");
		$(event.currentTarget).addClass("active");
	});
	
};