require("./containers.js");

var SwipeableMenu = require("./swipeable-menu.js");

var swipeableMenu = new SwipeableMenu();
//swipeableMenu.addDefaultShadow();

var editButton = $('.edit-button');
var editActionButtons = $('.edit-action-buttons');
var profileValues = $('.profile-info-values');
var profileInputs = $('.edit-inputs');
var saveButton = $('#saveButton');
var cancelButton = $('#cancelButton');
var editButtons = $('.edit-button-right');
var editableInfo = $('.editable-info');
var editTooltip = $('.edit-tooltip');
var editTooltipActionButtons = $('.save-button, .cancel-button');

var switchState = function() {
	[
		profileValues, 
		profileInputs, 
		editButton,
		editActionButtons
	].map(function(x) {
		x.toggleClass('hidden');
	});
};
 
[editButton, saveButton, cancelButton].map(function(x) {
	x.click(switchState);
});

editButtons.click(function(event) {
	editableInfo.removeClass('active');
	var parent = $(event.currentTarget).parent();
	parent.addClass('active');
	event.stopPropagation();
});

editTooltip.click(function(event) {
	event.stopPropagation();
});

editTooltipActionButtons.click(function(event) {
	editableInfo.removeClass('active');
});

$(document).click(function(event) {
	editableInfo.removeClass('active');
});