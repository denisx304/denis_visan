module.exports = (function() {
	var mdInputContainerSelector = "[md-input-container]";
	
	function getActionElements(elem) {
		var parent = elem.closest(mdInputContainerSelector);
		var label = parent.children('label');
		var placeholder = elem.attr("placeholder");
		
		return {
			label: label,
			placeholder: placeholder
		}
	};
	
	function activateLabel(elem, label) {
		if (elem.val() == "") {
			elem.removeAttr("placeholder");
			label.css("z-index", '2');
			label.addClass("active");
		} 
	};
	
	$(mdInputContainerSelector + " > input").keyup(function() {
		var _this = $(this);
		var elements = getActionElements(_this);
		
		activateLabel(_this, elements.label);
	});
	
	$(mdInputContainerSelector + " > input").focusin(function() {
		var _this = $(this);
		var elements = getActionElements(_this);
		
		_this.data("placeholder", elements.placeholder);
		elements.label.text(elements.placeholder);
			
		activateLabel(_this, elements.label);
	});
	
	$(mdInputContainerSelector + " > input").focusout(function() {
		var _this = $(this);
		var elements = getActionElements(_this);
		
		if (_this.val() == "") {
			elements.label.removeClass("active");
			window.setTimeout(function() {
				_this.attr("placeholder", _this.data("placeholder"));
				elements.label.text("");
				elements.label.css("z-index", '-1');
			}, 300);
		}
	});
	
})();