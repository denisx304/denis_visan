/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = (function() {
	var mdInputContainerSelector = "[md-input-container]";
	
	function getActionElements(elem) {
		var parent = elem.closest(mdInputContainerSelector);
		var label = parent.children('label');
		var placeholder = elem.attr("placeholder");
		
		return {
			label: label,
			placeholder: placeholder
		}
	};
	
	function activateLabel(elem, label) {
		if (elem.val() == "") {
			elem.removeAttr("placeholder");
			label.css("z-index", '2');
			label.addClass("active");
		} 
	};
	
	$(mdInputContainerSelector + " > input").keyup(function() {
		var _this = $(this);
		var elements = getActionElements(_this);
		
		activateLabel(_this, elements.label);
	});
	
	$(mdInputContainerSelector + " > input").focusin(function() {
		var _this = $(this);
		var elements = getActionElements(_this);
		
		_this.data("placeholder", elements.placeholder);
		elements.label.text(elements.placeholder);
			
		activateLabel(_this, elements.label);
	});
	
	$(mdInputContainerSelector + " > input").focusout(function() {
		var _this = $(this);
		var elements = getActionElements(_this);
		
		if (_this.val() == "") {
			elements.label.removeClass("active");
			window.setTimeout(function() {
				_this.attr("placeholder", _this.data("placeholder"));
				elements.label.text("");
				elements.label.css("z-index", '-1');
			}, 300);
		}
	});
	
})();

/***/ }),
/* 1 */,
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(0);

/***/ })
/******/ ]);