/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = (function() {
	var mdInputContainerSelector = "[md-input-container]";
	
	function getActionElements(elem) {
		var parent = elem.closest(mdInputContainerSelector);
		var label = parent.children('label');
		var placeholder = elem.attr("placeholder");
		
		return {
			label: label,
			placeholder: placeholder
		}
	};
	
	function activateLabel(elem, label) {
		if (elem.val() == "") {
			elem.removeAttr("placeholder");
			label.css("z-index", '2');
			label.addClass("active");
		} 
	};
	
	$(mdInputContainerSelector + " > input").keyup(function() {
		var _this = $(this);
		var elements = getActionElements(_this);
		
		activateLabel(_this, elements.label);
	});
	
	$(mdInputContainerSelector + " > input").focusin(function() {
		var _this = $(this);
		var elements = getActionElements(_this);
		
		_this.data("placeholder", elements.placeholder);
		elements.label.text(elements.placeholder);
			
		activateLabel(_this, elements.label);
	});
	
	$(mdInputContainerSelector + " > input").focusout(function() {
		var _this = $(this);
		var elements = getActionElements(_this);
		
		if (_this.val() == "") {
			elements.label.removeClass("active");
			window.setTimeout(function() {
				_this.attr("placeholder", _this.data("placeholder"));
				elements.label.text("");
				elements.label.css("z-index", '-1');
			}, 300);
		}
	});
	
})();

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = function SwipeableMenu() {
	
	this.container = $(".swipeable-menu-container");
	this.tabs = this.container.children();
	this.menu = $(".swipeable-menu");
	
	this.addDefaultShadow = function() {
		if (this.container.outerWidth() < this.container[0].scrollWidth) {
			this.menu.addClass('shadow-visible-right');
		}
	};
	
	this.getOffset = function(distance, coefficient) {
		return distance * coefficient;
	};	
	
	this.getLeftOffset = function(event, direction) {
		var time = event.swipestop.time - event.swipestart.time;
		var coefficient = 7;
		var left = parseFloat(this.container.css("left"));
		var offset = 0;
		var distance = 0;
		
		var maxOffset = this.container[0].scrollWidth - this.container.outerWidth();
		
		if (direction == "left") {
			distance = event.swipestart.coords[0] - event.swipestop.coords[0];
			offset = Math.max(-maxOffset, (left - this.getOffset(distance, coefficient))) + "px";
		} else if (direction == "right") {
			distance = event.swipestop.coords[0] - event.swipestart.coords[0];
			offset = Math.min((left + this.getOffset(distance, coefficient)), 0) + "px";
		}
		
		return {
			offset: offset,
			duration: time * coefficient / 10
		};
	};
	
	this.container.on("swipe", (event) => {
		var direction = "left";
		
		if (event.swipestart.coords[0] < event.swipestop.coords[0]) {
			direction = "right";
		}
		
		var swipeInfo = this.getLeftOffset(event, direction);
		
		this.container.animate({
			left: swipeInfo.offset
		}, swipeInfo.duration);
	});
	
	this.tabs.click((event) => {
		this.tabs.removeClass("active");
		$(event.currentTarget).addClass("active");
	});
	
};

/***/ }),
/* 2 */,
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(0);

var SwipeableMenu = __webpack_require__(1);

var swipeableMenu = new SwipeableMenu();
//swipeableMenu.addDefaultShadow();

var editButton = $('.edit-button');
var editActionButtons = $('.edit-action-buttons');
var profileValues = $('.profile-info-values');
var profileInputs = $('.edit-inputs');
var saveButton = $('#saveButton');
var cancelButton = $('#cancelButton');
var editButtons = $('.edit-button-right');
var editableInfo = $('.editable-info');
var editTooltip = $('.edit-tooltip');
var editTooltipActionButtons = $('.save-button, .cancel-button');

var switchState = function() {
	[
		profileValues, 
		profileInputs, 
		editButton,
		editActionButtons
	].map(function(x) {
		x.toggleClass('hidden');
	});
};
 
[editButton, saveButton, cancelButton].map(function(x) {
	x.click(switchState);
});

editButtons.click(function(event) {
	editableInfo.removeClass('active');
	var parent = $(event.currentTarget).parent();
	parent.addClass('active');
	event.stopPropagation();
});

editTooltip.click(function(event) {
	event.stopPropagation();
});

editTooltipActionButtons.click(function(event) {
	editableInfo.removeClass('active');
});

$(document).click(function(event) {
	editableInfo.removeClass('active');
});

/***/ })
/******/ ]);